Pour lancer le site :
-Récupérer le dossier dist.
-L'installer sur le serveur.

Le site devrait normalement être prêt au lancement.

Je n'ai malheuresement pas réussi à trouver comment build plusieurs pages, je n'ai donc que la page index dans le dist.

Pour voir à quoi ressemble le site complet :
-Télecharger le dossier carnetdevoyage complet.
-Ouvrir le terminal dedans.
-Taper : npm run dev.
-Aller sur le lien.